using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


    public class GameLoader : MonoBehaviour
    {
        
        public void tutorial()
        {
            SceneManager.LoadScene("Wham!");
        }
        
        public void lvl01()
        {
            SceneManager.LoadScene("");
        }
        
        public void lvl02()
        {
            SceneManager.LoadScene("Eiffel");
        }
        
        public void Dance()
        {
            SceneManager.LoadScene("Pickle");
        }
        
        public void Goa()
        {
            SceneManager.LoadScene("Timmy");
        }
        
        public void Reggae()
        {
            SceneManager.LoadScene("Bob Marley");
        }
        
        public void Menu()
        {
            SceneManager.LoadScene("Menu");
        }
        
        public void Intro()
        {
            SceneManager.LoadScene("Intro");
        }
    }

