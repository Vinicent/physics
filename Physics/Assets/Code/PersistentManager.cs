using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

namespace Physiks
{
    public class PersistentManager : MonoBehaviour
    {
        public static PersistentManager Instance { get; private set; }

        public int fuelAmount;

        private void Awake()
        {
            if (Instance != null)
            {
                Destroy(gameObject);
                fuelAmount = 0;
            }
            else
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
        }

        private void Update()
        {
            if(SceneManager.GetActiveScene().name == "Intro")
            {
                fuelAmount = 0;
            }
        }

        public void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("Fuel"))
            {
                fuelAmount += 3;
                Destroy(other.gameObject);
            }
        }
    }
}