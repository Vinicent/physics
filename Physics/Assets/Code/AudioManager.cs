﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CoATwoPR
{
    public class AudioManager : MonoBehaviour
    {
        public static AudioManager instance;

        void Awake()
        {
            if(instance == null)
            {
                instance = this;
                DontDestroyOnLoad(gameObject);
            }else if(instance != this)
            {
                Destroy(gameObject);
            }
        }
    }
}