using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PrefabSpawner : MonoBehaviour
{
    public GameObject prefab; // Das Prefab, das gespawnt wird
    public int numberOfPrefabs = 300; // Die Anzahl der zu spawnenden Prefabs
    public float spawnDelay = 0.5f; // Die Verzögerung zwischen den Spawns


    public void SpawnPrefabs()
    {
        StartCoroutine(SpawnPrefabsCoroutine());
    }

    private IEnumerator SpawnPrefabsCoroutine()
    {
        for (int i = 0; i < numberOfPrefabs; i++)
        {
            Instantiate(prefab, transform.position, Quaternion.identity);
            yield return new WaitForSeconds(spawnDelay);
        }
    }
}