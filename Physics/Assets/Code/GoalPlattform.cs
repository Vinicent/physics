using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Physiks
{
    public class GoalPlattform : MonoBehaviour
    {
        public float flySpeed;
        private bool m_FacingRight = true;

        public Vector2 pointA;
        public Vector2 pointB;
        public float time;

        public bool isMoving = true;

        private void Update()
        {
            if(isMoving == true)
            {
                time = Mathf.PingPong(Time.time * flySpeed, 1);
                transform.position = Vector3.Lerp(pointA, pointB, time);
            
                if (time > 0.99f && !m_FacingRight)
                {
                    Flip();
                }
                if (time < 0.01f && m_FacingRight)
                {
                    Flip();
                }
            }
        }

        public void OnTriggerEnter2D(Collider2D collision)
        {
            isMoving = false;
        }

        public void OnTriggerStay2D(Collider2D collision)
        {
            if (Input.GetKey(KeyCode.Space))
            {
                Debug.Log("Hello2");
                SceneManager.LoadScene("Outro");
            }
        }


        private void Flip()
        {
            m_FacingRight = !m_FacingRight;

            transform.Rotate(new Vector2(0f, 180f));
        }
    }
}