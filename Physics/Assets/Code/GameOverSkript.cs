using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Physiks
{
    public class GameOverSkript : MonoBehaviour
    {
        public GameObject gameOverScreen;

        public void OnTriggerEnter2D(Collider2D collision)
        {
            gameOverScreen.SetActive(true);
        }

        public void TryAgainButton()
        {
            SceneManager.LoadScene("FuelRace");
        }
        public void MainMenuButton()
        {
            SceneManager.LoadScene("Menu");
        }
        public void ResetButton()
        {
            SceneManager.LoadScene("FuelRace");
            PersistentManager.Instance.fuelAmount = 0;
        }
        public void TakeOffButton()
        {
            SceneManager.LoadScene("CutScene");
        }
    }
}
