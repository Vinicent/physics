using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveWalls : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            transform.position += transform.right * Time.deltaTime;
        }
        
        if (Input.GetMouseButton(1))
        {
            transform.position -= transform.right * Time.deltaTime;
        }
    }
}
