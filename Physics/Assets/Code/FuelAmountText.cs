using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Physiks
{
    public class FuelAmountText : MonoBehaviour
    {
        public TextMeshProUGUI textFuel;

        // Update is called once per frame
        void Update()
        {
            ChangeFuel();
        }
        public void ChangeFuel()
        {
            textFuel.text = PersistentManager.Instance.fuelAmount.ToString();
        }
    }
}
