using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Physiks
{
    public class PlayerCon : MonoBehaviour
    {
        public CharacterCon controllers;
        public float runSpeed = 40f;

        float horizontalMove = 0f;
        bool jump = false;
        public bool canMove = true;

        public int fuel;
        public AudioSource baah;
        public bool isLooping = false;

        public GameObject thrustSprite;
        // Update is called once per frame


        private void Start()
        {
            fuel = PersistentManager.Instance.fuelAmount;
        }
        void Update()
        {
            PersistentManager.Instance.fuelAmount = fuel;
            if (!canMove)
            {
            
                return;
            }

            horizontalMove = Input.GetAxis("Horizontal") * runSpeed;

            if (Input.GetButtonUp("Jump"))
            {
                thrustSprite.SetActive(false);
                baah.loop = false;
                baah.Stop();
                isLooping = false;
                //jump = true;
                //GetComponent<SpriteRenderer>().sprite = chargeSprite;
            }
            
            if (Input.GetButton("Jump") && fuel > 0)
            {
                fuel--;
                jump = true;
                thrustSprite.SetActive(true);
                if(isLooping == false)
                {
                    baah.Play();
                    baah.loop = true;
                    isLooping = true;
                }

            }

        }

        public void OnLanding()
        {
            /*
            if (Input.anyKey)
            {
                Debug.Log("Win");
                SceneManager.LoadScene("Outro");
            }
            */
        }
        void FixedUpdate()
        {
            if (!canMove)
            {
                return;
            }
            // Move our character
            controllers.Move(horizontalMove * Time.fixedDeltaTime, jump);
            
            jump = false;
        }
    }
}
