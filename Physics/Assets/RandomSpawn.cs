using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Physiks
{
    public class RandomSpawn : MonoBehaviour
    {
        public Transform rocket;
        public float randomx;
        public Vector2 pos;

        private void Start()
        {
            rocket = this.transform;
            randomx = Random.Range(8, -8);
            rocket.position = new Vector2(randomx, 5.57f);
            //pos = new Vector3(randomx, 5.57f, -7.5f);
            //this.transform.position = pos;

        }
    }
}
